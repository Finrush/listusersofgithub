import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import FollowerItem from '../components/FollowerItem'
import Header from '../components/Header'
import { MAIN_SCREEEN } from '../constants/routes'
import { clearFollowers } from '../redux/actions'

class DetailsScreen extends React.Component {
   
    _keyExtractor = (item, index) => item.id;
  
    _renderItem = ({item}) => (
      <FollowerItem
        id={item.id}
        login={item.login}
        htmlUrl={item.html_url}
        imgUrl={item.avatar_url}
      />
    );

    backPress = () => {
        const {navigation, clearFollowers} = this.props
        navigation.navigate(MAIN_SCREEEN)
        clearFollowers()
    }

  render() {
    const { container } = styles 
    const {data} = this.props
    return (
        <View >
            <Header 
                title={'User Followers List'}
                headerColor={'#9cf4bc'}
                back={true}
                onPress={() => this.backPress()}
            />
            <View style={container}>
            {
                data ? 
                <FlatList
                    data={data}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                /> : 
                <Text style={{fontSize: '20'}}>Loading...</Text>
            }
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20
    }
  })

const mapStateToProps = state => {
    return {
      data: state.search.followers
    }
  }
  
export default connect(mapStateToProps, {clearFollowers})(DetailsScreen)