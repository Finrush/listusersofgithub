import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import { connect } from 'react-redux'
import { getDatalist } from '../redux/actions'
import ListItem from '../components/ListItem'
import Header from '../components/Header'
import { DETAILS_SCREEN } from '../constants/routes'

class HomeScreen extends React.Component {
    
    componentDidMount = () => {
        this.props.getDatalist()
    }

    _keyExtractor = (item, index) => item.id;
  
    _onPressItem = () => {
        this.props.navigation.navigate(DETAILS_SCREEN)
    };
  
    _renderItem = ({item}) => (
      <ListItem
        id={item.id}
        onPressItem={this._onPressItem}
        login={item.login}
        htmlUrl={item.html_url}
        imgUrl={item.avatar_url}
      />
    );

  render() {
    const { container } = styles 
    const {data} = this.props
    return (
        <View >
            <Header 
                title={'Users List'}
                headerColor={'#4583e8'}
                back={false}
            />
            <View style={container}>
            {
                data ? 
                <FlatList
                    data={data}
                    keyExtractor={this._keyExtractor}
                    renderItem={this._renderItem}
                /> : 
                <Text style={{fontSize: '20'}}>Loading...</Text>
            }
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        justifyContent: 'center'
    }
  })

const mapStateToProps = state => {
    return {
      data: state.search.data
    }
  }
  
export default connect(mapStateToProps, { getDatalist })(HomeScreen)