import { createStackNavigator, createAppContainer } from 'react-navigation'
import Home from './Home'
import Details from './Detail'
import {
  MAIN_SCREEEN,
  DETAILS_SCREEN
} from '../constants/routes'

const AppNavigator = createStackNavigator(
  {
    [MAIN_SCREEEN]: Home,
    [DETAILS_SCREEN]: Details
  },
  {
    initialRouteName: MAIN_SCREEEN,
    headerMode: 'none'
  }
)
export default createAppContainer(AppNavigator);