import React from 'react'
import { TouchableOpacity, View, Text, StyleSheet, SafeAreaView, Image } from 'react-native'
import backIcon from '../../assets/image/backButton.png'

const Header = ({
  headerColor,
  title,
  onPress,
  back
}) => {
  const { viewStyle, textStyle, leftButtonStyle } = styles
  return (
    <SafeAreaView>
        <View style={[viewStyle, {backgroundColor: headerColor }]}>
        {back &&
            <TouchableOpacity onPress={onPress} style={{position: 'absolute',left: 35, top: 12}}>
              <Image style={leftButtonStyle} source={backIcon} /> 
            </TouchableOpacity>
        }
        <Text style={textStyle}>{title}</Text>
        </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  viewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 40,
    marginBottom: 10,
    position: 'relative'
  },
  textStyle: {
    color: '#fff',
    fontSize: 28,
    fontFamily: 'AvenirNext-DemiBold',
    paddingTop: 5
  },
  leftButtonStyle: {
    width: 20,
    height: 20,
  }
})

export default Header