import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { connect } from 'react-redux'
import { getUserFollowers } from '../redux/actions'

const ListItem = (props) => {
    _onPress = () => {
        const {login, getUserFollowers, onPressItem} = props
        getUserFollowers(login)
        onPressItem()
        
    };

    const {container, avatar, infoBlock, login} = styles
    return (
        <TouchableOpacity onPress={_onPress}>
            <View style={container}>
                <Image 
                    style={avatar}
                    source={{ uri: props.imgUrl }} 
                />
                <View style={infoBlock}>
                    <Text style={login}>Login: {props.login}</Text>
                    <Text>Profile link: {props.htmlUrl}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        borderColor: 'black',
        borderRadius: 5,
        borderWidth: 2,
        marginBottom: 10,
        padding: 5
    },
    infoBlock: {
        flex: 1,
        marginLeft: 10,
        paddingTop: 10
    },
    avatar: {
        width: 100, 
        height: 100
    },
    login:{
        marginBottom:10
    }
})
  
export default connect(null, { getUserFollowers })(ListItem)
