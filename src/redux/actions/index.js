import { 
  GET_DATA_FETCHED,
  GET_DATA_FAILED,
  USER_FOLLOWERS_FETCHED,
  USER_FOLLOWERS_FAILED,
  CLEAR_FOLLOWERS
} from '../types'

export const getDatalist = () => async (dispatch) => {
  function onSuccess(success) {
    dispatch({ type: GET_DATA_FETCHED, payload: success })
    return success
  }
  function onError(error) {
    dispatch({ type: GET_DATA_FAILED, error })
    return error
  }

  try {
    const URL = 'https://api.github.com/users?per_page=100&since=0'
    const res = await fetch(URL, {
      method: 'GET'
    })
    const success = await res.json()
    return onSuccess(success)
  } catch (error) {
    return onError(error)
  }
}

export const getUserFollowers = user => async (dispatch) => {
  function onSuccess(success) {
    dispatch({ type: USER_FOLLOWERS_FETCHED, payload: success })
    return success
  }
  function onError(error) {
    dispatch({ type: USER_FOLLOWERS_FAILED, error })
    return error
  }

  try {
    const URL = `https://api.github.com/users/${user}/followers?per_page=100`
    const res = await fetch(URL, {
      method: 'GET'
    })
    const success = await res.json()
    return onSuccess(success)
  } catch (error) {
    return onError(error)
  }
}

export const clearFollowers = () => {
  console.log('HERERERE')
  return { 
    type: CLEAR_FOLLOWERS 
  }
} 
