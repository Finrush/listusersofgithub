import { 
  GET_DATA_FETCHED,
  GET_DATA_FAILED,
  USER_FOLLOWERS_FETCHED,
  USER_FOLLOWERS_FAILED,
  CLEAR_FOLLOWERS
} from '../types'

const INITIAL_STATE = {
  followers: [],
  data: []
}

export default (state = INITIAL_STATE,  action) => {
  switch (action.type) {
    case GET_DATA_FETCHED:
      return {
        ...state,
        data: action.payload
      }
    case GET_DATA_FAILED:
      return {
        ...state
      }
    case USER_FOLLOWERS_FETCHED:
    return {
      ...state,
      followers: action.payload
    }
    case USER_FOLLOWERS_FAILED:
      return {
        ...state
    }
    case CLEAR_FOLLOWERS:
      return {
        ...state,
        followers: []
    }
    default: return state
  }
}
